﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Models
{
    public class Game
    {
        public int ID { get; set;}
        [Required]
        public string Venue { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string ZipCode { get; set; }
        [Required]
        public int NumberOfPlayer { get; set; }
        [Required]
        public string StartHour { get; set; }
        [Required]
        public string EndHour { get; set; }
        [Required]
        public int Duration { get; set; }
        [Required]
        public string Date { get; set; }
        [Required]
        public string CreationDate { get; set; }
        [Required]
        public ICollection<UserGame> UserGames { get; set; }
    }
}
