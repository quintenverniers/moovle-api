﻿using Moovle.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Repositories
{
    public interface ITeamStatRepository
    {
        Task<IEnumerable<TeamStatDTO>> GetTeamStats();
        Task<TeamStatDTO> GetTeamStat(int id);
        Task<TeamStatDTO> PostTeamStat(TeamStatDTO teamStatDTO);
        Task<TeamStatDTO> PutTeamStat(int id, TeamStatDTO teamStatDTO);
        Task<TeamStatDTO> DeleteTeamStat(int id);
    }
}
