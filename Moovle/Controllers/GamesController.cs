﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;
using Moovle.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Moovle.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly MoovleContext _context;
        private readonly IGameRepository _gameRepository;
        
        public GamesController(MoovleContext context, IGameRepository gameRepository)
        {
            _context = context;
            _gameRepository = gameRepository;
        }

        //GET: api/games
        /// <summary>
        /// Get all the games.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GameDTO>>> GetGames()
        {
            return Ok(await _gameRepository.GetGames().ConfigureAwait(false));
        }

        // GET: api/games/5
        /// <summary>
        /// Get the details of a specific game.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<GameDTO>> GetGame(int id)
        {
            return Ok(await _gameRepository.GetGame(id).ConfigureAwait(false));
        }

        /// POST: api/games
        /// <summary>
        /// Create a new game
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /api/games
        ///     {
        ///         {
	    ///             "Email": "test@test.com",
	    ///             "Password":"Azerty123!",
        ///             "Firstname": "Arno",
        ///             "UserName": "stasarno",
        ///             "Lastname": "Stas"
        ///        }
        ///     }
        /// </remarks>
        /// <param name="gameDTO"></param>
        /// <returns>A newly created game</returns>
        /// <response code="201">Returns the newly created game</response>
        /// <response code="400">If the item is null</response>
        [HttpPost]
        public async Task<ActionResult<GameDTO>> PostGame(GameDTO gameDTO)
        {
            /*var UserGames = _context.UserGames.Add(new UserGame()
            {
                GameId = gameDTO.UserGame.GameId,
                UserId = gameDTO.Players.UserId,
                Host = gameDTO.Players.Host
            });*/

            var gameResult = await _gameRepository.PostGame(gameDTO).ConfigureAwait(false);

            return CreatedAtAction(nameof(GetGame), new { id = gameDTO.ID }, gameDTO);
        } 

        [HttpPut("{id}")]
        public async Task<IActionResult> PutGame(int id, GameDTO gameDTO)
        {
            if (gameDTO == null) { throw new ArgumentNullException(nameof(gameDTO)); }

            if (id != gameDTO.ID)
            {
                return BadRequest();
            }

            var gameResult = await _gameRepository.PutGame(id, gameDTO).ConfigureAwait(false);

            if (gameResult == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<GameDTO>> DeleteGame(int id) {
            var gameResult = await _gameRepository.DeleteGame(id).ConfigureAwait(false);

            if(gameResult == null)
            {
                return NotFound();
            }

            return gameResult;
        }
    }
}
