﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.DTOs
{
    public class UserTeamDTO
    {
        public string UserID { get; set; }
        public int TeamID { get; set; }
    }
}
