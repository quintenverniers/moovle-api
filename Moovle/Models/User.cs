﻿ using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Avatar { get; set; }

        public ICollection<UserTeam> UserTeams { get; set; }
        public ICollection<UserMedal> UserMedals { get; set; }
        public ICollection<UserGame> UserGames { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
        public PlayerStat PlayerStat { get; set; } //navigation property
    }
}
