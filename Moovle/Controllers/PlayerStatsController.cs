﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;
using Moovle.Repositories;

namespace Moovle.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerStatsController : ControllerBase
    {
        private readonly MoovleContext _context;
        private readonly IPlayerStatRepository _playerStatRepository;

        public PlayerStatsController(MoovleContext context, IPlayerStatRepository playerStatRepository)
        {
            _context = context;
            _playerStatRepository = playerStatRepository;
        }

        // GET: api/PlayerStats
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerStatDTO>>> GetPlayerStats()
        {
            return Ok(await _playerStatRepository.GetPlayerStats().ConfigureAwait(false));
        }

        // GET: api/PlayerStats/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PlayerStatDTO>> GetPlayerStat(int id)
        {
            var playerStat = await _playerStatRepository.GetPlayerStat(id).ConfigureAwait(false);

            if (playerStat == null)
            {
                return NotFound();
            }

            return playerStat;
        }

        // PUT: api/PlayerStats/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayerStat(int id, PlayerStatDTO playerStatDTO)
        {
            if (playerStatDTO == null) { throw new ArgumentNullException(nameof(playerStatDTO)); }

            if (id != playerStatDTO.ID)
            {
                return BadRequest();
            }

            var pResult = await _playerStatRepository.PutPlayerStat(id, playerStatDTO).ConfigureAwait(false);

            if(pResult == null)
            {
                return NotFound();
            }
            return NoContent();
        }

        // POST: api/PlayerStats
        [HttpPost]
        public async Task<ActionResult<PlayerStatDTO>> PostPlayerStat(PlayerStatDTO playerStatDTO)
        {
            var statResult = await _playerStatRepository.PostPlayerStat(playerStatDTO).ConfigureAwait(false);

            return CreatedAtAction(nameof(GetPlayerStat), new { id = playerStatDTO.ID }, playerStatDTO);
        }

        // DELETE: api/PlayerStats/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PlayerStatDTO>> DeletePlayerStat(int id)
        {
            var playerStat = await _playerStatRepository.DeletePlayerStat(id).ConfigureAwait(false);

            if (playerStat == null)
            {
                return NotFound();
            }

            return playerStat;
        }

        private bool PlayerStatExists(int id)
        {
            return _context.PlayerStats.Any(e => e.ID == id);
        }
    }
}
