﻿using Moovle.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Repositories
{
    public interface ITeamRepository
    {
        Task<IEnumerable<TeamDTO>> GetTeams();
        Task<TeamDTO> GetTeam(int id);
        Task<TeamDTO> PostTeam(TeamDTO teamDTO);
        Task<TeamDTO> PutTeam(int id, TeamDTO teamDTO);
        Task<TeamDTO> DeleteTeam(int id);
    }
}
