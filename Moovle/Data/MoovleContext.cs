﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moovle.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Moovle.Data
{
    public class MoovleContext : IdentityDbContext
    <
        User,
        Role,
        string,
        IdentityUserClaim<string>,
        UserRole,
        IdentityUserLogin<string>,
        IdentityRoleClaim<string>,
        IdentityUserToken<string>
    >
    {

        public MoovleContext(DbContextOptions<MoovleContext> options) : base(options)
        {

        }

        public DbSet<Game> Games { get; set; }
        public DbSet<Medal> Medals { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<PlayerStat> PlayerStats { get; set; }
        public DbSet<TeamStat> TeamStats { get; set; }
        public DbSet<UserMedal> UserMedals { get; set; }
        public DbSet<UserTeam> UserTeams { get; set; }
        public DbSet<UserGame> UserGames { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Fluent API Configuration
            // ========================

            // UserMedals
            // ==================

            // Relations
            // ---------

            modelBuilder.Entity<UserMedal>()
                .HasKey(u => new { u.UserId, u.MedalId }); // Composite Primary Key

            modelBuilder.Entity<UserMedal>()
                .HasOne(u => u.User)
                .WithMany(u => u.UserMedals)
                .HasForeignKey(u => u.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict); // When a UserRoleAssignment is deleted, the corresponding User may not be deleted

            modelBuilder.Entity<UserMedal>()
                .HasOne(u => u.Medal)
                .WithMany(r => r.UserMedals)
                .HasForeignKey(u => u.MedalId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict); // When a UserRoleAssignment is deleted, the corresponding Role may not be deleted


            // UserTeam
            // ==================

            // Relations
            // ---------

            modelBuilder.Entity<UserTeam>()
                .HasKey(u => new { u.UserID, u.TeamID }); // Composite Primary Key


            modelBuilder.Entity<UserTeam>()
                .HasOne(u => u.User)
                .WithMany(t => t.UserTeams)
                .HasForeignKey(u => u.UserID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserTeam>()
                .HasOne(u => u.Team)
                .WithMany(u => u.UserTeams)
                .HasForeignKey(u => u.TeamID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);


            // PlayerStat
            // ===================

            //Relations
            // --------

            modelBuilder.Entity<User>()
                .HasOne(u => u.PlayerStat)
                .WithOne(u => u.User)
                .HasForeignKey<PlayerStat>(u => u.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            // TeamStat
            // ===================

            //Relations
            // --------

            modelBuilder.Entity<Team>()
                .HasOne(u => u.TeamStat)
                .WithOne(u => u.Team)
                .HasForeignKey<TeamStat>(u => u.TeamId)
                .OnDelete(DeleteBehavior.Restrict);



            // UserGames
            // ==================

            // Relations
            // ---------

            modelBuilder.Entity<UserGame>()
                .HasKey(u => new { u.UserId, u.GameId }); // Composite Primary Key

            modelBuilder.Entity<UserGame>()
                .HasOne(u => u.User)
                .WithMany(u => u.UserGames)
                .HasForeignKey(u => u.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict); // When a UserGame is deleted, the corresponding User may not be deleted

            modelBuilder.Entity<UserGame>()
                .HasOne(u => u.Game)
                .WithMany(r => r.UserGames)
                .HasForeignKey(u => u.GameId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict); // When a UserGame is deleted, the corresponding Game not be deleted

        }


    }
}
