﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;

namespace Moovle.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly MoovleContext _context;

        public RoleRepository(MoovleContext context)
        {
            _context = context;

            if (_context.Roles.Count() == 0)
            {
                _context.Roles.Add(new Role { Name = "Company Admin", Description = "Company administrator can manage company teams and host tournaments." });
                _context.SaveChanges();
            }
        }
        public Task<RoleDTO> DeleteRole(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<RoleDTO> GetRole(string id)
        {
            var role = await _context.Roles
               .Select(r => new RoleDTO()
               {
                   Id = r.Id,
                   Name = r.Name,
                   Description = r.Description,
               })
               .AsNoTracking()
               .FirstOrDefaultAsync(r => r.Id == id)
               .ConfigureAwait(false);

            if (role == null)
            {
                return null;
            }

            return role;
        }

        public async Task<IEnumerable<RoleDTO>> GetRoles()
        {
            return await _context.Roles.Select(r => new RoleDTO()
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description
            })
            .AsNoTracking()
            .ToListAsync()
            .ConfigureAwait(false);
        }

        public Task<RoleDTO> PostRole(RoleDTO roleDTO)
        {
            throw new NotImplementedException();
        }

        public async Task<RoleDTO> PutRole(string id, RoleDTO roleDTO)
        {
            if (roleDTO == null) { throw new ArgumentNullException(nameof(roleDTO)); }

            try
            {
                Role role = await _context.Roles.FirstOrDefaultAsync(r => r.Id == id);
                role.Name = roleDTO.Name;
                role.Description = roleDTO.Description;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return roleDTO;
        }

        private bool RoleExists(string id)
        {
            return _context.Roles.Any(e => e.Id == id);
        }
    }
}
