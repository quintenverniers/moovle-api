﻿using Moovle.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Repositories
{
    public interface IPlayerStatRepository
    {
        Task<IEnumerable<PlayerStatDTO>> GetPlayerStats();
        Task<PlayerStatDTO> GetPlayerStat(int id);
        Task<PlayerStatDTO> PostPlayerStat(PlayerStatDTO playerStatDTO);
        Task<PlayerStatDTO> PutPlayerStat(int id, PlayerStatDTO playerStatDTO);
        Task<PlayerStatDTO> DeletePlayerStat(int id);
    }
}
