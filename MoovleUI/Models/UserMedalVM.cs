﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class UserMedalVM
    {
        public string UserId { get; set; }
        public int MedalId { get; set; }
    }
}
