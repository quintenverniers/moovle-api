﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoovleUI.Models;

namespace MoovleUI.Controllers
{
    public class UsersController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public UsersController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        /*public async Task<IActionResult> Index()
        {
            Dictionary<string, string> dummy = TempData["StateData"] as Dictionary<string, string>;

            IEnumerable<UserVM> users = Array.Empty<UserVM>();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("users").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                users = await JsonSerializer.DeserializeAsync<IEnumerable<UserVM>>(responseStream);
                return View(users);
            }

            return RedirectOnError(response, "Index", "get all users", "getting all users");
        }*/

        public async Task<IActionResult> Details(string id)
        {
            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("users/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                var user = await JsonSerializer.DeserializeAsync<UserVM>(responseStream);
                return View(user);
            }

            return RedirectOnError(response, "Details", "get user details", "getting the user details");
        }

        public IActionResult Create()
        {
            return RedirectToRoute("register");
        }

        public async Task<IActionResult> Edit(string id)
        {

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("users/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                var game = await JsonSerializer.DeserializeAsync<UserPutVM>(responseStream);
                return View(game);
            }

            return RedirectOnError(response, "Edit", "edit a user", "getting the user details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, UserPutVM user)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var userContent = new StringContent(JsonSerializer.Serialize(user), Encoding.UTF8, "application/json");

                var response = await client.PutAsync("users/" + id, userContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    //return RedirectToAction(nameof(Index));
                    return RedirectToAction(nameof(Details), new { id = user.Id });
                }

                return RedirectOnError(response, "Edit", "edit a user", "saving the modified user");
            }

            return View(user);
        }

        public async Task<IActionResult> Delete(string id)
        {
            UserDeleteVM game = new UserDeleteVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("users/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                game = await JsonSerializer.DeserializeAsync<UserDeleteVM>(responseStream);
                return View(game);
            }

            return RedirectOnError(response, "Delete", "delete a game", "getting the game details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string id, UserDeleteVM game)
        {
            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.DeleteAsync("users/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction(nameof(Index));
            }

            return RedirectOnError(response, "Delete", "delete a game", "deleting the game");
        }

        private IActionResult RedirectOnError(HttpResponseMessage response, string actionMethod, string forbiddenMessage, string errorMessage)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                TempData["RedirectController"] = "Users";
                TempData["RedirectActionMethod"] = actionMethod;
                return RedirectToRoute("login");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                TempData["forbidden"] = true;
                TempData["forbiddenMessage"] = forbiddenMessage;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = true;
                TempData["errorMessage"] = errorMessage;
                return RedirectToAction("Index", "Home");
            }
        }
    }
}