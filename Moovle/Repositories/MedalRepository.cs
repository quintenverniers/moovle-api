﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;

namespace Moovle.Repositories
{
    public class MedalRepository : IMedalRepository
    {
        private readonly MoovleContext _context;

        public MedalRepository(MoovleContext context)
        {
            _context = context;

            if (_context.Medals.Count() == 0)
            {
                //Create new game if collection is empty
                _context.Medals.Add(new Medal { MedalName = "Rookie", MedalRequirements = "Play 10 Games" });
                _context.SaveChanges();
            }
        }
        public async Task<MedalDTO> DeleteMedal(int id)
        {
            var medal = await _context.Medals
                .Include(m => m.UserMedals)
                .FirstOrDefaultAsync(m => m.ID == id)
                .ConfigureAwait(false);

            if (medal == null)
            {
                return null;
            }

            _context.UserMedals.RemoveRange(medal.UserMedals);
            _context.Medals.Remove(medal);

            await _context.SaveChangesAsync().ConfigureAwait(false);

            return new MedalDTO()
            {
                ID = medal.ID,
                MedalName = medal.MedalName,
                MedalRequirements = medal.MedalRequirements,
            };
        }

        public async Task<MedalDTO> GetMedal(int id)
        {
            var medal = await _context.Medals
                .Include(m => m.UserMedals)
                .Select(m => new MedalDTO()
                {
                    ID = m.ID,
                    MedalName = m.MedalName,
                    MedalRequirements = m.MedalRequirements,
                    UserMedals = m.UserMedals.Select(x => new UserMedalDTO()
                    {
                        MedalId = x.MedalId,
                        UserId = x.UserId
                    }).ToList()
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.ID == id);

            if (medal == null)
            {
                return null;
            }

            return medal;
        }

        public async Task<IEnumerable<MedalDTO>> GetMedals()
        {
            return await _context.Medals
                .Include(m => m.UserMedals)
                .Select(m => new MedalDTO()
                {
                    ID = m.ID,
                    MedalName = m.MedalName,
                    MedalRequirements = m.MedalRequirements,
                    UserMedals = m.UserMedals.Select(x => new UserMedalDTO()
                    {
                        MedalId = x.MedalId,
                        UserId = x.UserId
                    }).ToList()
                })
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<MedalDTO> PostMedal(MedalDTO medalDTO)
        {
            var mResult = _context.Medals.Add(new Medal()
            {
                MedalName = medalDTO.MedalName,
                MedalRequirements = medalDTO.MedalRequirements,
            });

            await _context.SaveChangesAsync();

            medalDTO.ID = mResult.Entity.ID;

            return medalDTO;
        }

        public async Task<MedalDTO> PutMedal(int id, MedalDTO medalDTO)
        {
            if (medalDTO == null) { throw new ArgumentNullException(nameof(medalDTO)); }

            //_context.Entry(medal).State = EntityState.Modified;

            try
            {
                Medal medal = await _context.Medals.Include(m => m.UserMedals).FirstOrDefaultAsync(m => m.ID == medalDTO.ID);
                medal.MedalName = medalDTO.MedalName;
                medal.MedalRequirements = medalDTO.MedalRequirements;

                _context.UserMedals.RemoveRange(medal.UserMedals);

                foreach (UserMedalDTO userMedalDTO in medalDTO.UserMedals)
                {
                    User user = _context.Users.Find(userMedalDTO.UserId);
                    medal.UserMedals.Add(new UserMedal() { UserId = user.Id, MedalId = medal.ID });
                }

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedalExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return medalDTO;
        }

        private bool MedalExists(int id)
        {
            return _context.Medals.Any(e => e.ID == id);
        }
    }
}
