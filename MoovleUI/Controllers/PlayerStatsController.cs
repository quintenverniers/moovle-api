﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoovleUI.Models;

namespace MoovleUI.Controllers
{
    public class PlayerStatsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public PlayerStatsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IActionResult> Index()
        {
            Dictionary<string, string> dummy = TempData["StateData"] as Dictionary<string, string>;

            IEnumerable<PlayerStatVM> medals = Array.Empty<PlayerStatVM>();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("playerstats").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                medals = await JsonSerializer.DeserializeAsync<IEnumerable<PlayerStatVM>>(responseStream);
                return View(medals);
            }

            return RedirectOnError(response, "Index", "get all playerstats", "getting all playerstats");
        }

        public async Task<IActionResult> Details(int id)
        {
            PlayerStatVM playerstat = new PlayerStatVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("playerstats/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                playerstat = await JsonSerializer.DeserializeAsync<PlayerStatVM>(responseStream);
                return View(playerstat);
            }

            return RedirectOnError(response, "Details", "get playerstat details", "getting the playerstat details");
        }


        public async Task<IActionResult> Edit(int id)
        {
            PlayerStatVM playerstat = new PlayerStatVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("playerstats/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                playerstat = await JsonSerializer.DeserializeAsync<PlayerStatVM>(responseStream);
                return View(playerstat);
            }

            return RedirectOnError(response, "Edit", "edit a playerstat", "getting the playerstat details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PlayerStatVM playerstat)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var playerstatContent = new StringContent(JsonSerializer.Serialize(playerstat), Encoding.UTF8, "application/json");

                var response = await client.PutAsync("playerstats/" + id, playerstatContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Edit", "edit a playerstat", "saving the modified playerstat");
            }

            return View(playerstat);
        }

        private IActionResult RedirectOnError(HttpResponseMessage response, string actionMethod, string forbiddenMessage, string errorMessage)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                TempData["RedirectController"] = "PlayerStats";
                TempData["RedirectActionMethod"] = actionMethod;
                return RedirectToRoute("login");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                TempData["forbidden"] = true;
                TempData["forbiddenMessage"] = forbiddenMessage;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = true;
                TempData["errorMessage"] = errorMessage;
                return RedirectToAction("Index", "Home");
            }
        }
    }
}