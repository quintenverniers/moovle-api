﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Models
{
    public class UserTeam
    {
        public string UserID { get; set; }
        public User User { get; set; }
        public int TeamID { get; set; }
        public Team Team { get; set; }
    }
}
