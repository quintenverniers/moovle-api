﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.DTOs
{
    public class UserMedalDTO
    {
        public string UserId { get; set; }
        public int MedalId { get; set; }
    }
}
