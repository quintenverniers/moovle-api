﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.DTOs
{
    public class TeamDTO
    {
        public int ID { get; set; }
        
        [Required]
        public string Name { get; set; }
        [Required]
        public string CreationDate { get; set; }

        public ICollection<UserTeamDTO> TeamMembers { get; set; }
        public TeamStatDTO TeamStat { get; set; } //navigation property
    }
}
