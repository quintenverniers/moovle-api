﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class UserVM
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("email")]
        [Required]
        public string Email { get; set; }

        [JsonPropertyName("firstName")]
        [Required]
        public string FirstName { get; set; }

        [JsonPropertyName("lastName")]
        [Required]
        public string LastName { get; set; }

        [JsonPropertyName("userName")]
        [Required]
        public string UserName { get; set; }

        [JsonPropertyName("avatar")]
        public string Avatar { get; set; }

        [JsonPropertyName("teams")]
        public ICollection<UserTeamVM> Teams { get; set; }

        [JsonPropertyName("medals")]
        public ICollection<UserMedalVM> Medals { get; set; }
       
        [JsonPropertyName("games")]
        public ICollection<UserGameVM>Games { get; set; }

        [JsonPropertyName("roles")]
        public ICollection<UserRoleVM> Roles { get; set; }

        [JsonPropertyName("playerstat")]
        public PlayerStatVM PlayerStat { get; set; } //navigation property

        [JsonPropertyName("token")]
        public string Token { get; set; }
    }
}
