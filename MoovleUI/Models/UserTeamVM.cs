﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class UserTeamVM
    {
        public string UserID { get; set; }
        public int TeamID { get; set; }
    }
}
