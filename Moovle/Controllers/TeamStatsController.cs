﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;
using Moovle.Repositories;

namespace Moovle.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeamStatsController : ControllerBase
    {
        private readonly MoovleContext _context;
        private readonly ITeamStatRepository _teamStatRepository;

        public TeamStatsController(MoovleContext context, ITeamStatRepository teamStatRepository)
        {
            _context = context;
            _teamStatRepository = teamStatRepository;
        }

        // GET: api/TeamStats
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamStatDTO>>> GetTeamStats()
        {
            return Ok(await _teamStatRepository.GetTeamStats().ConfigureAwait(false));
        }

        // GET: api/TeamStats/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamStatDTO>> GetTeamStat(int id)
        {
            var teamStat = await _teamStatRepository.GetTeamStat(id).ConfigureAwait(false);

            if (teamStat == null)
            {
                return NotFound();
            }

            return teamStat;
        }

        // PUT: api/TeamStats/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeamStat(int id, TeamStatDTO teamStatDTO)
        {
            if (id != teamStatDTO.ID)
            {
                return BadRequest();
            }

            try
            {
                TeamStat teamStat = await _context.TeamStats.FirstOrDefaultAsync(t => t.ID == id);
                teamStat.Goals_scored = teamStatDTO.Goals_scored;
                teamStat.Assists = teamStatDTO.Assists;
                teamStat.Minuted_played = teamStatDTO.Minuted_played;
                teamStat.Games_played = teamStatDTO.Games_played;
                teamStat.Games_won = teamStatDTO.Games_won;
                teamStat.Games_lost = teamStatDTO.Games_lost;
                teamStat.Games_draw = teamStatDTO.Games_draw;

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeamStatExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TeamStats
        [HttpPost]
        public async Task<ActionResult<TeamStatDTO>> PostTeamStat(TeamStatDTO teamStatDTO)
        {
            var statResult = _context.TeamStats.Add(new TeamStat()
            {
                Goals_scored = teamStatDTO.Goals_scored,
                Assists = teamStatDTO.Assists,
                Minuted_played = teamStatDTO.Minuted_played,
                Games_played = teamStatDTO.Games_played,
                Games_won = teamStatDTO.Games_won,
                Games_lost = teamStatDTO.Games_lost,
                Games_draw = teamStatDTO.Games_draw,
                TeamId = 1,
            });
            await _context.SaveChangesAsync();

            teamStatDTO.ID = statResult.Entity.ID;

            return CreatedAtAction(nameof(GetTeamStat), new { id = teamStatDTO.ID }, teamStatDTO);
        }

        // DELETE: api/TeamStats/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TeamStatDTO>> DeleteTeamStat(int id)
        {
            var teamStat = await _context.TeamStats
                .FirstOrDefaultAsync(t => t.ID == id);

            if (teamStat == null)
            {
                return NotFound();
            }

            _context.TeamStats.Remove(teamStat);
            await _context.SaveChangesAsync();

            return new TeamStatDTO()
            {
                Goals_scored = teamStat.Goals_scored,
                Assists = teamStat.Assists,
                Minuted_played = teamStat.Minuted_played,
                Games_played = teamStat.Games_played,
                Games_won = teamStat.Games_won,
                Games_lost = teamStat.Games_lost,
                Games_draw = teamStat.Games_draw
            };
        }

        private bool TeamStatExists(int id)
        {
            return _context.TeamStats.Any(e => e.ID == id);
        }
    }
}
