﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Models
{

        public class Role : IdentityRole
        {
            public string Description { get; set; }

            // A Role can be assigned to many Users
            public ICollection<UserRole> UserRoles { get; set; }
        }
    }
