﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class GameVM
    {
        [JsonPropertyName("id")]
        public int ID { get; set; }

        [JsonPropertyName("venue")]
        [Display(Name = "Venue")]
        [Required(ErrorMessage = "A game must have a venue.")]
        public string Venue { get; set; }

        [JsonPropertyName("address")]
        [Display(Name = "Address")]
        [Required(ErrorMessage ="Please provide an address")]
        public string Address { get; set; }

        [JsonPropertyName("city")]
        [Display(Name = "City")]
        [Required(ErrorMessage ="City is required")]
        public string City { get; set; }

        [JsonPropertyName("zipCode")]
        [Display(Name = "Zip code")]
        [Required(ErrorMessage ="Zip code is required")]
        public string ZipCode { get; set; }

        [JsonPropertyName("numberOfPlayer")]
        [Display(Name = "Participants")]
        [Required(ErrorMessage = "Number of players is required")]
        public int NumberOfPlayer { get; set; }

        [JsonPropertyName("startHour")]
        [Display(Name = "Start hour")]
        [Required(ErrorMessage = "Start hour is required")]
        public string StartHour { get; set; }

        [JsonPropertyName("endHour")]
        [Display(Name = "End hour")]
        [Required(ErrorMessage = "End hour is required" )]
        public string EndHour { get; set; }

        [JsonPropertyName("duration")]
        [Display(Name = "Duration")]
        [Required(ErrorMessage = "Duration is required")]
        public int Duration { get; set; }

        [JsonPropertyName("date")]
        [Display(Name = "Date")]
        [Required(ErrorMessage = "Date is required")]
        public string Date { get; set; }

        [JsonPropertyName("creationDate")]
        [Required]
        public string CreationDate { get; set; }

        [JsonPropertyName("players")]
        public ICollection<UserGameVM> Players { get; set; }
    }
}
