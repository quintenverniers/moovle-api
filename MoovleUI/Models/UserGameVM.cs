﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class UserGameVM
    {
        public string UserId { get; set; }
        public int GameId { get; set; } 
        public Boolean Host { get; set; }
    }
}
