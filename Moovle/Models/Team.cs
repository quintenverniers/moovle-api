﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Models
{
    public class Team
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string CreationDate { get; set; }
        public ICollection<UserTeam> UserTeams { get; set; }
        public TeamStat TeamStat { get; set; } //navigation property

    }
}
