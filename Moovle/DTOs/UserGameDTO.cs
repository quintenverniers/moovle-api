﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.DTOs
{
    public class UserGameDTO
    {
        public string UserId { get; set; }
        public int GameId { get; set; } 
        public Boolean Host { get; set; }
    }
}
