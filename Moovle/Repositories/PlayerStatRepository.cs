﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;

namespace Moovle.Repositories
{
    public class PlayerStatRepository : IPlayerStatRepository
    {
        private readonly MoovleContext _context;

        public PlayerStatRepository(MoovleContext context)
        {
            _context = context;

            if (_context.PlayerStats.Count() == 0)
            {
                _context.PlayerStats.Add(new PlayerStat { Goals_scored = 5, Assists = 11, Games_played = 35, Games_lost = 5, Games_draw = 8, Games_won = 22, Minuted_played = 2100, UserId = "1" });
                _context.SaveChanges();
            }
        }
        public async Task<PlayerStatDTO> DeletePlayerStat(int id)
        {
            var playerStat = await _context.PlayerStats
                .FirstOrDefaultAsync(t => t.ID == id).ConfigureAwait(false);

            if (playerStat == null)
            {
                return null;
            }

            _context.PlayerStats.Remove(playerStat);
            await _context.SaveChangesAsync().ConfigureAwait(false);

            return new PlayerStatDTO()
            {
                Goals_scored = playerStat.Goals_scored,
                Assists = playerStat.Assists,
                Minuted_played = playerStat.Minuted_played,
                Games_played = playerStat.Games_played,
                Games_won = playerStat.Games_won,
                Games_lost = playerStat.Games_lost,
                Games_draw = playerStat.Games_draw
            };
        }

        public async Task<PlayerStatDTO> GetPlayerStat(int id)
        {
            var playerStat = await _context.PlayerStats
                .Select(t => new PlayerStatDTO()
                {
                    ID = t.ID,
                    Goals_scored = t.Goals_scored,
                    Assists = t.Assists,
                    Games_played = t.Games_played,
                    Games_lost = t.Games_lost,
                    Games_draw = t.Games_draw,
                    Games_won = t.Games_won,
                    Minuted_played = t.Minuted_played,
                    UserId = t.UserId,
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(p => p.ID == id)
                .ConfigureAwait(false);

            return playerStat;
        }

        public async Task<IEnumerable<PlayerStatDTO>> GetPlayerStats()
        {
            return await _context.PlayerStats
                .Select(t => new PlayerStatDTO()
                {
                    ID = t.ID,
                    Goals_scored = t.Goals_scored,
                    Assists = t.Assists,
                    Games_played = t.Games_played,
                    Games_lost = t.Games_lost,
                    Games_draw = t.Games_draw,
                    Games_won = t.Games_won,
                    Minuted_played = t.Minuted_played,
                    UserId = t.UserId,
                })
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<PlayerStatDTO> PostPlayerStat(PlayerStatDTO playerStatDTO)
        {
            var statResult = _context.PlayerStats.Add(new PlayerStat()
            {
                Goals_scored = playerStatDTO.Goals_scored,
                Assists = playerStatDTO.Assists,
                Minuted_played = playerStatDTO.Minuted_played,
                Games_played = playerStatDTO.Games_played,
                Games_won = playerStatDTO.Games_won,
                Games_lost = playerStatDTO.Games_lost,
                Games_draw = playerStatDTO.Games_draw,
                UserId = playerStatDTO.UserId,
            }); ;
            await _context.SaveChangesAsync().ConfigureAwait(false);
            playerStatDTO.ID = statResult.Entity.ID;

            return playerStatDTO;
        }

        public async Task<PlayerStatDTO> PutPlayerStat(int id, PlayerStatDTO playerStatDTO)
        {
            if (playerStatDTO == null) { throw new ArgumentNullException(nameof(playerStatDTO)); }

            try
            {
                PlayerStat playerStat = await _context.PlayerStats.FirstOrDefaultAsync(t => t.ID == id).ConfigureAwait(false);
                playerStat.Goals_scored = playerStatDTO.Goals_scored;
                playerStat.Assists = playerStatDTO.Assists;
                playerStat.Minuted_played = playerStatDTO.Minuted_played;
                playerStat.Games_played = playerStatDTO.Games_played;
                playerStat.Games_won = playerStatDTO.Games_won;
                playerStat.Games_lost = playerStatDTO.Games_lost;
                playerStat.Games_draw = playerStatDTO.Games_draw;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerStatExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return playerStatDTO;
        }

        private bool PlayerStatExists(int id)
        {
            return _context.PlayerStats.Any(e => e.ID == id);
        }
    }
}
