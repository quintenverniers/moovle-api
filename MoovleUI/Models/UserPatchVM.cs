﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class UserPatchVM
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("currentPassword")]
        [Required]
        public string CurrentPassword { get; set; }

        [JsonPropertyName("newPassword")]
        [Required]
        public string NewPassword { get; set; }
    }
}
