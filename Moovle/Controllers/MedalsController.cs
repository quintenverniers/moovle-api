﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;
using Moovle.Repositories;

namespace Moovle.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class MedalsController : ControllerBase
    {
        private readonly MoovleContext _context;
        private readonly IMedalRepository _medalRepository;

        public MedalsController(MoovleContext context, IMedalRepository medalRepository)
        {
            _context = context;
            _medalRepository = medalRepository;
        }

        // GET: api/Medals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MedalDTO>>> GetMedals()
        {
            return Ok(await _medalRepository.GetMedals().ConfigureAwait(false));
        }

        // GET: api/Medals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MedalDTO>> GetMedal(int id)
        {
            var medal = await _medalRepository.GetMedal(id).ConfigureAwait(false);

            if (medal == null)
            {
                return NotFound();
            }

            return medal;
        }

        // PUT: api/Medals/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedal(int id, MedalDTO medalDTO)
        {
            if (medalDTO == null) { throw new ArgumentNullException(nameof(medalDTO)); }

            if (id != medalDTO.ID)
            {
                return BadRequest();
            }

            var medalResult = await _medalRepository.PutMedal(id, medalDTO).ConfigureAwait(false);

            if (medalResult == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Medals
        [HttpPost]
        public async Task<ActionResult<MedalDTO>> PostMedal(MedalDTO medalDTO)
        {
            var mResult = await _medalRepository.PostMedal(medalDTO).ConfigureAwait(false);

            return CreatedAtAction(nameof(GetMedal), new { id = medalDTO.ID }, medalDTO);
        }

        // DELETE: api/Medals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MedalDTO>> DeleteMedal(int id)
        {
            var medal = await _medalRepository.DeleteMedal(id).ConfigureAwait(false);

            if (medal == null)
            {
                return NotFound();
            }

            return medal;
        }
    }
}
