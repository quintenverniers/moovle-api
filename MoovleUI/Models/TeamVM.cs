﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class TeamVM
    {
        [JsonPropertyName("id")]
        public int ID { get; set; }

        [JsonPropertyName("name")]
        [Required]
        public string Name { get; set; }

        [JsonPropertyName("creationDate")]
        [Required]
        public string CreationDate { get; set; }
        [JsonPropertyName("teamMembers")]
        public ICollection<UserTeamVM> TeamMembers { get; set; }
        [JsonPropertyName("teamStat")]
        public TeamStatVM TeamStat { get; set; } //navigation property
    }
}
