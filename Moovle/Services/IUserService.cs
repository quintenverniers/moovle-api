﻿using Moovle.DTOs;
using Moovle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Moovle.Services
{
    public interface IUserService
    {
        Task<UserDTO> Login(string username, string password);
        string GetUserId(ClaimsPrincipal userClaims);
        User GetUserByEmail(string email);
        User GetUserById(string id);
        Task<User> GetUserByUserName(string userName);
    }
}
