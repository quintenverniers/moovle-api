﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Models
{
    public class UserMedal
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public int MedalId { get; set; }
        public Medal Medal { get; set; }
    }
}
