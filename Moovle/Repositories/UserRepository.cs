﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;

namespace Moovle.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly MoovleContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public UserRepository(MoovleContext context, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;

            /*if (_context.Users.Count() == 0)
            {
                //Create new game if collection is empty
                //_context.Users.Add(new User { Email = "quintenverniers@gmail.com", Password = "Azerty123", FirstName="Quinten",LastName="Verniers", UserName="Quinten Verniers", Avatar="somelink", PlayerStat = { Goals_scored = 10, Assists = 3, Games_played = 5, Games_won = 4, Games_lost = 0, Games_draw = 1} });
                _context.SaveChanges();
            }*/
        }
        public async Task<UserDeleteDTO> DeleteUser(string id)
        {
            var user = await _context.Users
                .Include(c => c.UserGames)
                .Include(c => c.UserMedals)
                .Include(c => c.UserTeams)
                .Include(c => c.UserRoles)
                .Include(c => c.PlayerStat)
                .FirstOrDefaultAsync(c => c.Id == id)
                .ConfigureAwait(false);

            if (user == null)
            {
                return null;
            }

            _ = user.PlayerStat != null ? _context.PlayerStats.Remove(user.PlayerStat) : null;
            _context.UserGames.RemoveRange(user.UserGames);
            _context.UserMedals.RemoveRange(user.UserMedals);
            _context.UserTeams.RemoveRange(user.UserTeams);
            _context.UserRoles.RemoveRange(user.UserRoles);
            _context.PlayerStats.Remove(user.PlayerStat);
            _context.Users.Remove(user);

            await _context.SaveChangesAsync().ConfigureAwait(false);

            return new UserDeleteDTO()
            {
                ID = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Email = user.Email,
            };
        }

        public async Task<UserDTO> GetUserDetails(string id)
        {
            var user = await _context.Users
                .Include(i => i.UserGames)
                .Include(j => j.UserMedals)
                .Include(k => k.UserTeams)
                .Include(l => l.UserRoles)
                .Include(m => m.PlayerStat)
                .Select(u => new UserDTO()
                {
                    Id = u.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    UserName = u.UserName,
                    Avatar = u.Avatar,
                    Games = u.UserGames.Select(x => new UserGameDTO()
                    {
                        GameId = x.GameId,
                        UserId = x.UserId,
                        Host = x.Host
                    }).ToList(),
                    Teams = u.UserTeams.Select(x => new UserTeamDTO()
                    {
                        TeamID = x.TeamID,
                        UserID = x.UserID
                    }).ToList(),
                    Medals = u.UserMedals.Select(x => new UserMedalDTO()
                    {
                        MedalId = x.MedalId,
                        UserId = x.UserId,
                    }).ToList(),
                    Roles = u.UserRoles.Select(x => new UserRoleDTO()
                    {
                        Name = x.Role.Name,
                        Description = x.Role.Description,
                    }).ToList(),
                    PlayerStat = new PlayerStatDTO()
                    {
                        ID = u.PlayerStat.ID,
                        UserId = u.Id,
                        Goals_scored = u.PlayerStat.Goals_scored,
                        Assists = u.PlayerStat.Assists,
                        Minuted_played = u.PlayerStat.Minuted_played,
                        Games_played = u.PlayerStat.Games_played,
                        Games_won = u.PlayerStat.Games_won,
                        Games_draw = u.PlayerStat.Games_draw,
                        Games_lost = u.PlayerStat.Games_lost,
                    }
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id == id)
                .ConfigureAwait(false);

            if (user == null)
            {
                return null;
            }

            return user;
        }

        public async Task<IEnumerable<UserDTO>> GetUsers()
        {
            return await _context.Users
                .Include(i => i.UserGames)
                .Include(j => j.UserMedals)
                .Include(k => k.UserTeams)
                .Include(l => l.UserRoles)
                .Include(m => m.PlayerStat)
                .Select(u => new UserDTO()
                {
                    Id = u.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    UserName = u.UserName,
                    Avatar = u.Avatar,
                    Games = u.UserGames.Select(x => new UserGameDTO()
                    {
                        GameId = x.GameId,
                        UserId = x.UserId,
                        Host = x.Host
                    }).ToList(),
                    Teams = u.UserTeams.Select(x => new UserTeamDTO()
                    {
                        TeamID = x.TeamID,
                        UserID = x.UserID
                    }).ToList(),
                    Medals = u.UserMedals.Select(x => new UserMedalDTO()
                    {
                        MedalId = x.MedalId,
                        UserId = x.UserId,
                    }).ToList(),
                    Roles = u.UserRoles.Select(x => new UserRoleDTO()
                    {
                        Name = x.Role.Name,
                        Description = x.Role.Description,
                    }).ToList(),
                    PlayerStat = new PlayerStatDTO()
                    {
                        ID = u.PlayerStat.ID,
                        UserId = u.Id,
                        Goals_scored = u.PlayerStat.Goals_scored,
                        Assists = u.PlayerStat.Assists,
                        Minuted_played = u.PlayerStat.Minuted_played,
                        Games_played = u.PlayerStat.Games_played,
                        Games_won = u.PlayerStat.Games_won,
                        Games_draw = u.PlayerStat.Games_draw,
                        Games_lost = u.PlayerStat.Games_lost,
                    }
                })
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<UserPatchDTO> PatchUser(string id, UserPatchDTO userPatchDTO)
        {
            if (userPatchDTO == null) { throw new ArgumentNullException(nameof(userPatchDTO)); }

            try
            {
                // User user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id).ConfigureAwait(false);
                User user = await _userManager.FindByIdAsync(id).ConfigureAwait(false);
                var result = await _userManager.ChangePasswordAsync(user, userPatchDTO.CurrentPassword, userPatchDTO.NewPassword).ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await UserExists(id).ConfigureAwait(false) == false)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return userPatchDTO;
        }

        public async Task<UserPostDTO> PostUser(UserPostDTO userPostDTO)
        {
            var uResult = new User()
            {
                FirstName = userPostDTO.FirstName,
                LastName = userPostDTO.LastName,
                UserName = userPostDTO.UserName,
                Email = userPostDTO.Email,
            };

            var result = await _userManager.CreateAsync(uResult, userPostDTO.Password).ConfigureAwait(false);

            PlayerStat playerStat = new PlayerStat
            {
                Goals_scored = 0,
                Assists = 0,
                Minuted_played = 0,
                Games_played = 0,
                Games_won = 0,
                Games_lost = 0,
                Games_draw = 0,
                UserId = uResult.Id,
            };

            var pResult = _context.PlayerStats.Add(playerStat);
            await _context.SaveChangesAsync().ConfigureAwait(false);

            userPostDTO.Id = uResult.Id;

            return userPostDTO;
        }

        public async Task<UserPutDTO> PutUser(string id, UserPutDTO userPutDTO)
        {
            if (userPutDTO == null) { throw new ArgumentNullException(nameof(userPutDTO)); }

            //_context.Entry(user).State = EntityState.Modified;

            try
            {
                User user = await _context.Users
                    .Include(u => u.UserRoles)
                    .Include(u => u.UserTeams)
                    .Include(u => u.UserMedals)
                    .Include(u => u.UserGames)
                    .Include(u => u.PlayerStat)
                    .FirstOrDefaultAsync(u => u.Id == id)
                    .ConfigureAwait(false);

                user.FirstName = userPutDTO.FirstName;
                user.LastName = userPutDTO.LastName;
                user.UserName = userPutDTO.UserName;
                user.Email = userPutDTO.Email;
                user.Avatar = userPutDTO.Avatar;

                _context.UserRoles.RemoveRange(user.UserRoles);
                var roles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
                await _userManager.RemoveFromRolesAsync(user, roles.ToArray()).ConfigureAwait(false);

                await _userManager.AddToRolesAsync(user, userPutDTO.Roles).ConfigureAwait(false);
                await _userManager.UpdateAsync(user).ConfigureAwait(false);

                _context.UserMedals.RemoveRange(user.UserMedals);
                foreach (UserMedalDTO userMedalDTO in userPutDTO.Medals)
                {
                    Medal medal = _context.Medals.Find(userMedalDTO.MedalId);
                    user.UserMedals.Add(new UserMedal() { UserId = user.Id, MedalId = medal.ID });
                }

                _context.UserGames.RemoveRange(user.UserGames);
                foreach (UserGameDTO userGameDTO in userPutDTO.Games)
                {
                    Game game = _context.Games.Find(userGameDTO.GameId);
                    user.UserGames.Add(new UserGame() { UserId = user.Id, GameId = game.ID });
                }

                _context.UserTeams.RemoveRange(user.UserTeams);
                foreach (UserTeamDTO userTeamDTO in userPutDTO.Teams)
                {
                    Team team = _context.Teams.Find(userTeamDTO.TeamID);
                    user.UserTeams.Add(new UserTeam() { UserID = user.Id, TeamID = team.ID });
                }

                /*user.PlayerStat.Minuted_played = userPutDTO.PlayerStat.Minuted_played;
                user.PlayerStat.Games_played = userPutDTO.PlayerStat.Games_played;
                user.PlayerStat.Games_won = userPutDTO.PlayerStat.Games_won;
                user.PlayerStat.Games_lost = userPutDTO.PlayerStat.Games_lost;
                user.PlayerStat.Games_draw = userPutDTO.PlayerStat.Games_draw;
                user.PlayerStat.Assists = userPutDTO.PlayerStat.Assists;
                user.PlayerStat.Goals_scored = userPutDTO.PlayerStat.Goals_scored;*/

                await _context.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await UserExists(id).ConfigureAwait(false) == false)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return userPutDTO;
        }

        public async Task<UserRegisterDTO> RegisterUser(UserRegisterDTO userRegisterDTO)
        {
            if (userRegisterDTO == null) { throw new ArgumentNullException(nameof(userRegisterDTO)); }

            User user = new User
            {
                UserName = userRegisterDTO.Username,
                Email = userRegisterDTO.Email,
            };

            var registerResult = await _userManager.CreateAsync(user, userRegisterDTO.Password).ConfigureAwait(false);

            if (registerResult.Succeeded)
            {
                userRegisterDTO.Id = user.Id;


                return userRegisterDTO;
            }

            return null;
        }

        private async Task<bool> UserExists(string id)
        {
            return await _userManager.FindByIdAsync(id).ConfigureAwait(false) != null ? true : false;
        }
    }
}
