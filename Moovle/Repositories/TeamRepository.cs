﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;

namespace Moovle.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private readonly MoovleContext _context;

        public TeamRepository(MoovleContext context)
        {
            _context = context;
            /*if (_context.Teams.Count() == 0)
            {
                //Create new game if collection is empty
                _context.Teams.Add(new Team { Name = "Team 1", CreationDate = "30/09/2019", TeamStat = { Goals_scored = 1, Assists = 0, Games_played = 3, Games_lost = 3, Games_draw = 0, Games_won = 0, Minuted_played = 60, TeamId = 1 } });
                _context.SaveChanges();
            }*/
        }
        public async Task<TeamDTO> DeleteTeam(int id)
        {
            var team = await _context.Teams
                 .Include(t => t.UserTeams)
                 .Include(t => t.TeamStat)
                 .FirstOrDefaultAsync(t => t.ID == id);

            if (team == null)
            {
                return null;
            }

            _context.UserTeams.RemoveRange(team.UserTeams);
            _context.TeamStats.Remove(team.TeamStat);
            _context.Teams.Remove(team);

            await _context.SaveChangesAsync();

            return new TeamDTO()
            {
                ID = team.ID,
                Name = team.Name,
                CreationDate = team.CreationDate,
            };
        }

        public async Task<TeamDTO> GetTeam(int id)
        {
            var team = await _context.Teams
                .Include(t => t.UserTeams)
                .Include(t => t.TeamStat)
                .Select(x => new TeamDTO()
                {
                    ID = x.ID,
                    Name = x.Name,
                    CreationDate = x.CreationDate,
                    TeamMembers = x.UserTeams.Select(y => new UserTeamDTO()
                    {
                        TeamID = y.TeamID,
                        UserID = y.UserID
                    }).ToList(),
                    TeamStat = new TeamStatDTO()
                    {
                        ID = x.TeamStat.ID,
                        TeamId = x.ID,
                        Goals_scored = x.TeamStat.Goals_scored,
                        Assists = x.TeamStat.Assists,
                        Minuted_played = x.TeamStat.Minuted_played,
                        Games_played = x.TeamStat.Games_played,
                        Games_won = x.TeamStat.Games_won,
                        Games_draw = x.TeamStat.Games_draw,
                        Games_lost = x.TeamStat.Games_lost,
                    }
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ID == id)
                .ConfigureAwait(false);

            if (team == null)
            {
                return null;
            }

            return team;
        }

        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            return await _context.Teams
                .Include(t => t.UserTeams)
                .Include(t => t.TeamStat)
                .Select(x => new TeamDTO()
                {
                    ID = x.ID,
                    Name = x.Name,
                    CreationDate = x.CreationDate,
                    TeamMembers = x.UserTeams.Select(y => new UserTeamDTO()
                    {
                        TeamID = y.TeamID,
                        UserID = y.UserID
                    }).ToList(),
                    TeamStat = new TeamStatDTO()
                    {
                        ID = x.TeamStat.ID,
                        TeamId = x.ID,
                        Goals_scored = x.TeamStat.Goals_scored,
                        Assists = x.TeamStat.Assists,
                        Minuted_played = x.TeamStat.Minuted_played,
                        Games_played = x.TeamStat.Games_played,
                        Games_won = x.TeamStat.Games_won,
                        Games_draw = x.TeamStat.Games_draw,
                        Games_lost = x.TeamStat.Games_lost,
                    }
                })
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<TeamDTO> PostTeam(TeamDTO teamDTO)
        {
            var teamResult = _context.Teams.Add(new Team()
            {
                Name = teamDTO.Name,
                CreationDate = teamDTO.CreationDate,
            });

            await _context.SaveChangesAsync().ConfigureAwait(false);

            var teamStat = _context.TeamStats.Add(new TeamStat()
            {
                Games_played = 0,
                Games_won = 0,
                Games_lost = 0,
                Games_draw = 0,
                Assists = 0,
                Minuted_played = 0,
                Goals_scored = 0,
                TeamId = teamResult.Entity.ID,
            });

            await _context.SaveChangesAsync().ConfigureAwait(false);
            teamDTO.ID = teamResult.Entity.ID;

            return teamDTO;
        }

        public async Task<TeamDTO> PutTeam(int id, TeamDTO teamDTO)
        {
            if (teamDTO == null) { throw new ArgumentNullException(nameof(teamDTO)); }
            
            try
            {
                Team team = await _context.Teams
                    .Include(t => t.UserTeams)
                    .Include(t => t.TeamStat)
                    .FirstOrDefaultAsync(x => x.ID == id);

                team.Name = teamDTO.Name;

                _context.UserTeams.RemoveRange(team.UserTeams);
                foreach (UserTeamDTO userTeamDTO in teamDTO.TeamMembers)
                {
                    User user = _context.Users.Find(userTeamDTO.UserID);
                    team.UserTeams.Add(new UserTeam() { UserID = user.Id, TeamID = team.ID });
                }

               /* team.TeamStat.Minuted_played = teamDTO.TeamStat.Minuted_played;
                team.TeamStat.Games_played = teamDTO.TeamStat.Games_played;
                team.TeamStat.Games_won = teamDTO.TeamStat.Games_won;
                team.TeamStat.Games_lost = teamDTO.TeamStat.Games_lost;
                team.TeamStat.Games_draw = teamDTO.TeamStat.Games_draw;
                team.TeamStat.Assists = teamDTO.TeamStat.Assists;
                team.TeamStat.Goals_scored = teamDTO.TeamStat.Goals_scored;*/

                await _context.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeamExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return teamDTO;
        }

        private bool TeamExists(int id)
        {
            return _context.Teams.Any(e => e.ID == id);
        }
    }
}
