﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.Models;
using Moovle.DTOs;
using Moovle.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace Moovle.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly MoovleContext _context;
        private readonly IRoleRepository _roleRepository;

        public RolesController(MoovleContext context, IRoleRepository roleRepository)
        {
            _context = context;
            _roleRepository = roleRepository;
        }

        // GET: api/Roles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoleDTO>>> GetRole()
        {
            return Ok(await _roleRepository.GetRoles().ConfigureAwait(false));
        }

        // GET: api/Roles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RoleDTO>> GetRole(string id)
        {
            var role = await _roleRepository.GetRole(id).ConfigureAwait(false);

            if (role == null)
            {
                return NotFound();
            }

            return role;
        }

        // PUT: api/Roles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRole(string id, RoleDTO roleDTO)
        {
            if (roleDTO == null) { throw new ArgumentNullException(nameof(roleDTO)); }

            if(id != roleDTO.Id)
            {
                return BadRequest();
            }

            var roleResult = await _roleRepository.PutRole(id, roleDTO).ConfigureAwait(false);

            if (roleResult == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Roles
        [HttpPost]
        public async Task<ActionResult<RoleDTO>> PostRole(RoleDTO role)
        {
            var roleResult = _context.Roles.Add(new Role()
            {
                Name = role.Name,
                Description = role.Description,
            });
            
            await _context.SaveChangesAsync();
            role.Id = roleResult.Entity.Id;

            return CreatedAtAction(nameof(GetRole), new { id = role.Id }, role);
        }

        // DELETE: api/Roles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RoleDTO>> DeleteRole(string id)
        {
            var role = await _context.Roles
                .Include(r => r.UserRoles)
                .FirstOrDefaultAsync(r => r.Id == id);

            if (role == null)
            {
                return NotFound();
            }

            _context.UserRoles.RemoveRange(role.UserRoles);
            _context.Roles.Remove(role);
            
            await _context.SaveChangesAsync();

            return new RoleDTO()
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description,
            };
        }
    }
}
