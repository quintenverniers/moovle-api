﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;

namespace Moovle.Repositories
{
    public class TeamStatRepository : ITeamStatRepository
    {
        private readonly MoovleContext _context;

        public TeamStatRepository(MoovleContext context)
        {
            _context = context;

            if (_context.TeamStats.Count() == 0)
            {
                _context.TeamStats.Add(new TeamStat { Goals_scored = 1, Assists = 0, Games_played = 3, Games_lost = 3, Games_draw = 0, Games_won = 0, Minuted_played = 60, TeamId = 1 });
                _context.SaveChanges();
            }
        }
        public Task<TeamStatDTO> DeleteTeamStat(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<TeamStatDTO> GetTeamStat(int id)
        {
            var teamStat = await _context.TeamStats
                .Select(t => new TeamStatDTO()
                {
                    ID = t.ID,
                    Goals_scored = t.Goals_scored,
                    Assists = t.Assists,
                    Games_played = t.Games_played,
                    Games_lost = t.Games_lost,
                    Games_draw = t.Games_draw,
                    Games_won = t.Games_won,
                    Minuted_played = t.Minuted_played,
                    TeamId = t.TeamId,
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(t => t.ID == id)
                .ConfigureAwait(false);

            if(teamStat == null)
            {
                return null;
            }

            return teamStat;
        }

        public async Task<IEnumerable<TeamStatDTO>> GetTeamStats()
        {
            return await _context.TeamStats
                .Select(t => new TeamStatDTO()
                {
                    ID = t.ID,
                    Goals_scored = t.Goals_scored,
                    Assists = t.Assists,
                    Games_played = t.Games_played,
                    Games_lost = t.Games_lost,
                    Games_draw = t.Games_draw,
                    Games_won = t.Games_won,
                    Minuted_played = t.Minuted_played,
                    TeamId = t.TeamId,
                })
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public Task<TeamStatDTO> PostTeamStat(TeamStatDTO teamStatDTO)
        {
            throw new NotImplementedException();
        }

        public Task<TeamStatDTO> PutTeamStat(int id, TeamStatDTO teamStatDTO)
        {
            throw new NotImplementedException();
        }
    }
}
