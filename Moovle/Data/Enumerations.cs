﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Data
{
    public class Enumerations
    {
        public enum IndoorPitchType { Grass, Concrete, Gym }
        public enum OutdoorPitchType { Grass, Concrete, Gravel }
    }
}
