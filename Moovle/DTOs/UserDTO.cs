﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.DTOs
{
    public class UserDTO
    {
        public string Id { get; set; }

        [Required]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string UserName { get; set; }
        public string Avatar { get; set; }

        public ICollection<UserTeamDTO> Teams { get; set; }
        public ICollection<UserMedalDTO> Medals { get; set; }
        public ICollection<UserGameDTO>Games { get; set; }
        public ICollection<UserRoleDTO> Roles { get; set; }
        public PlayerStatDTO PlayerStat { get; set; } //navigation property
        public string Token { get; set; }
    }
}
