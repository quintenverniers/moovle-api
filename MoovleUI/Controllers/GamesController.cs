﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoovleUI.Models;

namespace MoovleUI.Controllers
{
    public class GamesController : Controller
    {

        private readonly IHttpClientFactory _httpClientFactory;
        public GamesController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IActionResult> Index()
        {
            Dictionary<string, string> dummy = TempData["StateData"] as Dictionary<string, string>;

            IEnumerable<GameVM> games = Array.Empty<GameVM>();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("games").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                games = await JsonSerializer.DeserializeAsync<IEnumerable<GameVM>>(responseStream);
                return View(games);
            }
            
            return RedirectOnError(response, "Index", "get all games", "getting all games");
        }

        public async Task<IActionResult> Details(int id) {
            GameVM game = new GameVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("games/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                game = await JsonSerializer.DeserializeAsync<GameVM>(responseStream);
                return View(game);
            }

            return RedirectOnError(response, "Details", "get address details", "getting the address details");
        }
       
        public async Task<IActionResult> Create() {
            if (!HttpContext.Session.Keys.Contains("users"))
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var response = await client.GetAsync("users").ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    HttpContext.Session.SetString("users", responseString);
                }
                else
                {
                    return RedirectOnError(response, "Create", "create a game", "getting the users");
                }
            }

            return View();
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GameVM game) {
            game.CreationDate = "20/08/2020";
            game.NumberOfPlayer = 0;
            game.Players = new List<UserGameVM>();
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var gameContent = new StringContent(JsonSerializer.Serialize(game), Encoding.UTF8, "application/json");

                var response = await client.PostAsync("games", gameContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Create", "create a game", "creating a game");
            }

            return View(game);
        }

        public async Task<IActionResult> Edit(int id) {
            GameVM game = new GameVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("games/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                game = await JsonSerializer.DeserializeAsync<GameVM>(responseStream);
                return View(game);
            }

            return RedirectOnError(response, "Edit", "edit a game", "getting the game details");
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, GameVM game) {
            game.Players = new List<UserGameVM>();
            game.NumberOfPlayer = 0;
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var gameContent = new StringContent(JsonSerializer.Serialize(game), Encoding.UTF8, "application/json");

                var response = await client.PutAsync("games/" + id, gameContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Edit", "edit a game", "saving the modified game");
            }

            return View(game);
        }

        public async Task<IActionResult> Delete(int id) {
            GameVM game = new GameVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("games/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                game = await JsonSerializer.DeserializeAsync<GameVM>(responseStream);
                return View(game);
            }

            return RedirectOnError(response, "Delete", "delete a game", "getting the game details");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id, GameVM game) {
            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.DeleteAsync("games/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction(nameof(Index));
            }

            return RedirectOnError(response, "Delete", "delete a game", "deleting the game");
        }

        private IActionResult RedirectOnError(HttpResponseMessage response, string actionMethod, string forbiddenMessage, string errorMessage)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                TempData["RedirectController"] = "Games";
                TempData["RedirectActionMethod"] = actionMethod;
                return RedirectToRoute("login");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                TempData["forbidden"] = true;
                TempData["forbiddenMessage"] = forbiddenMessage;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = true;
                TempData["errorMessage"] = errorMessage;
                return RedirectToAction("Index", "Home");
            }
        }


    }
}