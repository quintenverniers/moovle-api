﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;
using Moovle.Repositories;
using Moovle.Services;

namespace Moovle.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly MoovleContext _context;
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;

        public UsersController(MoovleContext context, IUserRepository userRepository, IUserService userService)
        {
            _context = context;
            _userRepository = userRepository;
            _userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            return Ok(await _userRepository.GetUsers().ConfigureAwait(false));
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUser(string id)
        {
            //var user = await _context.Users.FindAsync(id);
            var user = Ok(await _userRepository.GetUserDetails(id).ConfigureAwait(false));

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(string id, UserPutDTO userPutDTO)
        {
            userPutDTO.Teams = new List<UserTeamDTO>();
            userPutDTO.Medals = new List<UserMedalDTO>();
            userPutDTO.Games = new List<UserGameDTO>();
            userPutDTO.Roles = new string[] { };
            if (IsCurrentUser(id))
            {
                if (userPutDTO == null) { throw new ArgumentNullException(nameof(userPutDTO)); }

                if (id != userPutDTO.Id)
                {
                    return BadRequest();
                }

                var userResult = await _userRepository.PutUser(id, userPutDTO).ConfigureAwait(false);

                if (userResult == null)
                {
                    return NotFound();
                }

                return NoContent();

            }
            return Forbid(JwtBearerDefaults.AuthenticationScheme);
        }

        // PATCH: api/Users/5/ChangePassword
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchUser(string id, UserPatchDTO userPatchDTO)
        {
            if (IsCurrentUser(id)) {
                if (userPatchDTO == null) { throw new ArgumentNullException(nameof(userPatchDTO)); }

                if (id != userPatchDTO.Id)
                {
                    return BadRequest();
                }

                var userResult = await _userRepository.PatchUser(id, userPatchDTO).ConfigureAwait(false);

                if (userResult == null)
                {
                    return NotFound();
                }

                return NoContent();
            }
            return Forbid(JwtBearerDefaults.AuthenticationScheme);
        }

        // POST: api/Users
        [HttpPost]
        public async Task<ActionResult<UserPostDTO>> PostUser(UserPostDTO userPostDTO)
        {
            var uResult = Ok(await _userRepository.PostUser(userPostDTO).ConfigureAwait(false));

            return CreatedAtAction(nameof(GetUser), new { id = userPostDTO.Id }, userPostDTO);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserDeleteDTO>> DeleteUser(string id)
        {
            var userResult = await _userRepository.DeleteUser(id).ConfigureAwait(false);

            if (userResult == null)
            {
                return NotFound();
            }

            return userResult;
        }

        // POST: api/Users/login
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<UserDTO>> Login(UserLoginDTO userLoginDTO)
        {
            var userResult = await _userService.Login(userLoginDTO.UserName, userLoginDTO.Password).ConfigureAwait(false);

            if (userResult == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            
            return CreatedAtAction("GetUser", new { id = userResult.Id }, userResult);
        }

        // POST: api/Users/register
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult<UserDTO>> Register(UserRegisterDTO userRegisterDTO)
        {
            try
            {
                var userNameExists = await _userService.GetUserByUserName(userRegisterDTO.Username).ConfigureAwait(false);

                if (userNameExists != null)
                {
                    return BadRequest(new { message = "The user name already exists" });
                }

                var userEmailExists = _userService.GetUserByEmail(userRegisterDTO.Email);

                if (userEmailExists != null)
                {
                    return BadRequest(new { message = "This email is linked to an existing account" });
                }

                var userResult = await _userRepository.RegisterUser(userRegisterDTO).ConfigureAwait(false);

                if (userResult == null)
                {
                    return BadRequest(new { message = "Registration failed" });
                }

                UserLoginDTO userLoginDTO = new UserLoginDTO
                {
                    UserName = userRegisterDTO.Username,
                    Password = userRegisterDTO.Password
                };

                return await Login(userLoginDTO).ConfigureAwait(false);
            }
            catch(Exception e)
            {
                return BadRequest(new { message = "Something went horribly wrong." });
            }
        }

        private bool IsCurrentUser(string id)
        {
            return User.FindFirstValue(ClaimTypes.Name) == id;
        }
    }
}
