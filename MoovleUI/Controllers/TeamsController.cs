﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoovleUI.Models;

namespace MoovleUI.Controllers
{
    public class TeamsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public TeamsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IActionResult> Index()
        {
            Dictionary<string, string> dummy = TempData["StateData"] as Dictionary<string, string>;

            IEnumerable<TeamVM> teams = Array.Empty<TeamVM>();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("teams").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                teams = await JsonSerializer.DeserializeAsync<IEnumerable<TeamVM>>(responseStream);
                return View(teams);
            }

            return RedirectOnError(response, "Index", "get all teams", "getting all teams");
        }

        public async Task<IActionResult> Details(int id)
        {
            TeamVM team = new TeamVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("teams/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                team = await JsonSerializer.DeserializeAsync<TeamVM>(responseStream);
                return View(team);
            }

            return RedirectOnError(response, "Details", "get team details", "getting the team details");
        }

        public async Task<IActionResult> Create()
        {
            if (!HttpContext.Session.Keys.Contains("users"))
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var response = await client.GetAsync("users").ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    HttpContext.Session.SetString("users", responseString);
                }
                else
                {
                    return RedirectOnError(response, "Create", "create a team", "getting the users");
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TeamVM team)
        {
            team.CreationDate = "20/08/2020";
            Console.WriteLine(team);
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var teamContent = new StringContent(JsonSerializer.Serialize(team), Encoding.UTF8, "application/json");

                var response = await client.PostAsync("teams", teamContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Create", "create a team", "creating a team");
            }

            return View(team);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TeamVM team = new TeamVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("teams/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                team = await JsonSerializer.DeserializeAsync<TeamVM>(responseStream);
                return View(team);
            }

            return RedirectOnError(response, "Edit", "edit a team", "getting the team details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, TeamVM team)
        {
            team.CreationDate = "20/08/2020";
            team.TeamMembers = new List<UserTeamVM>();
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var teamContent = new StringContent(JsonSerializer.Serialize(team), Encoding.UTF8, "application/json");

                var response = await client.PutAsync("teams/" + id, teamContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Edit", "edit a team", "saving the modified team");
            }

            return View(team);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TeamVM team = new TeamVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("teams/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                team = await JsonSerializer.DeserializeAsync<TeamVM>(responseStream);
                return View(team);
            }

            return RedirectOnError(response, "Delete", "delete a team", "getting the team details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id, MedalVM team)
        {
            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.DeleteAsync("teams/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction(nameof(Index));
            }

            return RedirectOnError(response, "Delete", "delete a team", "deleting the team");
        }

        private IActionResult RedirectOnError(HttpResponseMessage response, string actionMethod, string forbiddenMessage, string errorMessage)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                TempData["RedirectController"] = "Teams";
                TempData["RedirectActionMethod"] = actionMethod;
                return RedirectToRoute("login");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                TempData["forbidden"] = true;
                TempData["forbiddenMessage"] = forbiddenMessage;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = true;
                TempData["errorMessage"] = errorMessage;
                return RedirectToAction("Index", "Home");
            }
        }
    }
}