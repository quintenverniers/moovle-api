﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Models
{
    public class TeamStat
    {
        public int ID { get; set; }
        public int Goals_scored { get; set; }
        public int Assists { get; set; }
        public int Minuted_played { get; set; }
        public int Games_played { get; set; }
        public int Games_won { get; set; }
        public int Games_lost { get; set; }
        public int Games_draw { get; set; }
        
        public int TeamId { get; set; }
        public Team Team { get; set; } // Navigation Property
    }
}
