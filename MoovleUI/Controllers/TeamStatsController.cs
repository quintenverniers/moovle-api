﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoovleUI.Models;

namespace MoovleUI.Controllers
{
    public class TeamStatsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public TeamStatsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IActionResult> Index()
        {
            Dictionary<string, string> dummy = TempData["StateData"] as Dictionary<string, string>;

            IEnumerable<TeamStatVM> medals = Array.Empty<TeamStatVM>();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("teamstats").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                medals = await JsonSerializer.DeserializeAsync<IEnumerable<TeamStatVM>>(responseStream);
                return View(medals);
            }

            return RedirectOnError(response, "Index", "get all teamstats", "getting all teamstats");
        }

        public async Task<IActionResult> Details(int id)
        {
            TeamStatVM teamstat = new TeamStatVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("teamstats/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                teamstat = await JsonSerializer.DeserializeAsync<TeamStatVM>(responseStream);
                return View(teamstat);
            }

            return RedirectOnError(response, "Details", "get teamstat details", "getting the teamstat details");
        }


        public async Task<IActionResult> Edit(int id)
        {
            TeamStatVM teamstat = new TeamStatVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("teamstats/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                teamstat = await JsonSerializer.DeserializeAsync<TeamStatVM>(responseStream);
                return View(teamstat);
            }

            return RedirectOnError(response, "Edit", "edit a teamstat", "getting the teamstat details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, TeamStatVM teamstat)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var teamstatContent = new StringContent(JsonSerializer.Serialize(teamstat), Encoding.UTF8, "application/json");

                var response = await client.PutAsync("teamstats/" + id, teamstatContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Edit", "edit a teamstat", "saving the modified teamstat");
            }

            return View(teamstat);
        }

        private IActionResult RedirectOnError(HttpResponseMessage response, string actionMethod, string forbiddenMessage, string errorMessage)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                TempData["RedirectController"] = "TeamStats";
                TempData["RedirectActionMethod"] = actionMethod;
                return RedirectToRoute("login");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                TempData["forbidden"] = true;
                TempData["forbiddenMessage"] = forbiddenMessage;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = true;
                TempData["errorMessage"] = errorMessage;
                return RedirectToAction("Index", "Home");
            }
        }
    }
}