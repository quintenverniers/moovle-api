﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;

namespace Moovle.Repositories
{
    public class GameRepository : IGameRepository
    {
        private readonly MoovleContext _context;

        public GameRepository(MoovleContext context)
        {
            _context = context;

            if (_context.Games.Count() == 0)
            {
                //Create new game if collection is empty
                _context.Games.Add(new Game { Venue = "Park Lokeren", Address = "Doorslaarstraat 57", City = "Lokeren", ZipCode = "9160", NumberOfPlayer = 10, StartHour = "16:00", EndHour = "17:00", Duration = 60, Date = "1/10/2019", CreationDate = "30/09/2019" });
                _context.SaveChanges();
            }
        }

        public async Task<GameDTO> DeleteGame(int id)
        {
            var game = await _context.Games
                .Include(g => g.UserGames)
                .FirstOrDefaultAsync(g => g.ID == id)
                .ConfigureAwait(false);

            if (game == null)
            {
                return null;
            }

            _context.UserGames.RemoveRange(game.UserGames);
            _context.Games.Remove(game);
            await _context.SaveChangesAsync().ConfigureAwait(false); 

            return new GameDTO()
            {
                ID = game.ID,
                Venue = game.Venue,
                Address = game.Address,
                City = game.City,
                ZipCode = game.ZipCode,
                NumberOfPlayer = game.NumberOfPlayer,
                StartHour = game.StartHour,
                EndHour = game.StartHour,
                Duration = game.Duration,
                Date = game.Date,
                CreationDate = game.CreationDate,
            };
        }

        public async Task<GameDTO> GetGame(int id)
        {
            var game = await _context.Games
               .Include(i => i.UserGames)
               .Select(g => new GameDTO()
               {
                   ID = g.ID,
                   Venue = g.Venue,
                   Address = g.Address,
                   City = g.City,
                   ZipCode = g.ZipCode,
                   NumberOfPlayer = g.NumberOfPlayer,
                   StartHour = g.StartHour,
                   EndHour = g.StartHour,
                   Duration = g.Duration,
                   Date = g.Date,
                   CreationDate = g.CreationDate,
                   Players = g.UserGames.Select(x => new UserGameDTO()
                   {
                       GameId = x.GameId,
                       UserId = x.UserId,
                       Host = x.Host
                   }).ToList()
               })
               .AsNoTracking()
               .FirstOrDefaultAsync(c => c.ID == id)
                .ConfigureAwait(false);

            if (game == null)
            {
                return null;
            }

            return game;
        }

        public async Task<IEnumerable<GameDTO>> GetGames()
        {
            return await _context.Games
                .Include(i => i.UserGames)
                .Select(g => new GameDTO()
                {
                    ID = g.ID,
                    Venue = g.Venue,
                    Address = g.Address,
                    City = g.City,
                    ZipCode = g.ZipCode,
                    NumberOfPlayer = g.NumberOfPlayer,
                    StartHour = g.StartHour,
                    EndHour = g.StartHour,
                    Duration = g.Duration,
                    Date = g.Date,
                    CreationDate = g.CreationDate,
                    Players = g.UserGames.Select(x => new UserGameDTO()
                    {
                        GameId = x.GameId,
                        UserId = x.UserId,
                        Host = x.Host
                    }).ToList()
                })
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<GameDTO> PostGame(GameDTO gameDTO)
        {
            if (gameDTO == null) { throw new ArgumentNullException(nameof(gameDTO)); }

            var gameResult = _context.Games.Add(new Game()
            {
                Venue = gameDTO.Venue,
                Address = gameDTO.Address,
                City = gameDTO.City,
                ZipCode = gameDTO.ZipCode,
                NumberOfPlayer = gameDTO.NumberOfPlayer,
                StartHour = gameDTO.StartHour,
                EndHour = gameDTO.StartHour,
                Duration = gameDTO.Duration,
                Date = gameDTO.Date,
                CreationDate = gameDTO.CreationDate,
            });

            await _context.SaveChangesAsync();
            gameDTO.ID = gameResult.Entity.ID;

            return gameDTO;
        }

        public async Task<GameDTO> PutGame(int id, GameDTO gameDTO)
        {

            if (gameDTO == null) { throw new ArgumentNullException(nameof(gameDTO)); }
            
            /*if (id != gameDTO.ID)
            {
                return BadRequest();
            }*/

            try
            {
                Game game = await _context.Games.Include(g => g.UserGames).FirstOrDefaultAsync(g => g.ID == id);
                game.Venue = gameDTO.Venue;
                game.Address = gameDTO.Address;
                game.City = gameDTO.City;
                game.ZipCode = gameDTO.ZipCode;
                game.NumberOfPlayer = gameDTO.NumberOfPlayer;
                game.StartHour = gameDTO.StartHour;
                game.EndHour = gameDTO.StartHour;
                game.Duration = gameDTO.Duration;
                game.Date = gameDTO.Date;

                _context.UserGames.RemoveRange(game.UserGames);

                foreach (UserGameDTO userGameDTO in gameDTO.Players)
                {
                    User user = _context.Users.Find(userGameDTO.UserId);
                    game.UserGames.Add(new UserGame() { GameId = game.ID, UserId = user.Id });
                }

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return gameDTO;
        }

        private bool GameExists(int id)
        {
            return _context.Games.Any(e => e.ID == id);
        }
    }
}
