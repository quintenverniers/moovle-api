﻿using Moovle.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Repositories
{
    public interface IGameRepository
    {
        Task<IEnumerable<GameDTO>> GetGames();
        Task<GameDTO> GetGame(int id);
        Task<GameDTO> PostGame(GameDTO gameDTO);
        Task<GameDTO> PutGame(int id, GameDTO gameDTO);
        Task<GameDTO> DeleteGame(int id);
    }
}
