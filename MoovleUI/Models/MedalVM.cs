﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MoovleUI.Models
{
    public class MedalVM
    {
        [JsonPropertyName("id")]
        public int ID { get; set; }

        [JsonPropertyName("medalName")]
        [Required]
        public string MedalName { get; set; }

        [JsonPropertyName("medalRequirements")]
        public string MedalRequirements { get; set; }
        public ICollection<UserMedalVM> UserMedals { get; set; }
    }
}
