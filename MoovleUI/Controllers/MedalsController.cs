﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoovleUI.Models;

namespace MoovleUI.Controllers
{
    public class MedalsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public MedalsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<IActionResult> Index()
        {
            Dictionary<string, string> dummy = TempData["StateData"] as Dictionary<string, string>;

            IEnumerable<MedalVM> medals = Array.Empty<MedalVM>();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("medals").ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                medals = await JsonSerializer.DeserializeAsync<IEnumerable<MedalVM>>(responseStream);
                return View(medals);
            }

            return RedirectOnError(response, "Index", "get all medals", "getting all medals");
        }

        public async Task<IActionResult> Details(int id)
        {
            MedalVM medal = new MedalVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("medals/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                medal = await JsonSerializer.DeserializeAsync<MedalVM>(responseStream);
                return View(medal);
            }

            return RedirectOnError(response, "Details", "get medal details", "getting the medal details");
        }

        public async Task<IActionResult> Create()
        {
            if (!HttpContext.Session.Keys.Contains("users"))
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var response = await client.GetAsync("users").ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    HttpContext.Session.SetString("users", responseString);
                }
                else
                {
                    return RedirectOnError(response, "Create", "create a medal", "getting the users");
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MedalVM medal)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var medalContent = new StringContent(JsonSerializer.Serialize(medal), Encoding.UTF8, "application/json");

                var response = await client.PostAsync("medals", medalContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Create", "create a medal", "creating a medal");
            }

            return View(medal);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MedalVM medal = new MedalVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("medals/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                medal = await JsonSerializer.DeserializeAsync<MedalVM>(responseStream);
                return View(medal);
            }

            return RedirectOnError(response, "Edit", "edit a medal", "getting the medal details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, MedalVM medal)
        {
            if (ModelState.IsValid)
            {
                var client = _httpClientFactory.CreateClient("Moovle");

                var medalContent = new StringContent(JsonSerializer.Serialize(medal), Encoding.UTF8, "application/json");

                var response = await client.PutAsync("medals/" + id, medalContent).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectOnError(response, "Edit", "edit a medal", "saving the modified medal");
            }

            return View(medal);
        }

        public async Task<IActionResult> Delete(int id)
        {
            MedalVM medal = new MedalVM();

            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.GetAsync("medals/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                medal = await JsonSerializer.DeserializeAsync<MedalVM>(responseStream);
                return View(medal);
            }

            return RedirectOnError(response, "Delete", "delete a medal", "getting the medal details");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id, MedalVM medal)
        {
            var client = _httpClientFactory.CreateClient("Moovle");

            var response = await client.DeleteAsync("medals/" + id).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction(nameof(Index));
            }

            return RedirectOnError(response, "Delete", "delete a medal", "deleting the medal");
        }

        private IActionResult RedirectOnError(HttpResponseMessage response, string actionMethod, string forbiddenMessage, string errorMessage)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                TempData["RedirectController"] = "Medals";
                TempData["RedirectActionMethod"] = actionMethod;
                return RedirectToRoute("login");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                TempData["forbidden"] = true;
                TempData["forbiddenMessage"] = forbiddenMessage;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["error"] = true;
                TempData["errorMessage"] = errorMessage;
                return RedirectToAction("Index", "Home");
            }
        }
    }
}