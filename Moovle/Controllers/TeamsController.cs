﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moovle.Data;
using Moovle.DTOs;
using Moovle.Models;
using Moovle.Repositories;

namespace Moovle.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly MoovleContext _context;
        private readonly ITeamRepository _teamRepository;

        public TeamsController(MoovleContext context, ITeamRepository teamRepository)
        {
            _context = context;
            _teamRepository = teamRepository;
        }

        // GET: api/Teams
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeams()
        {
            return Ok(await _teamRepository.GetTeams().ConfigureAwait(false));
        }

        // GET: api/Teams/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetTeam(int id)
        {
            var team = Ok(await _teamRepository.GetTeam(id).ConfigureAwait(false));
            
            if (team == null)
            {
                return NotFound();
            }

            return team;
        }

        // PUT: api/Teams/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeam(int id, TeamDTO teamDTO)
        {
            if (teamDTO == null) { throw new ArgumentNullException(nameof(teamDTO)); }

            if (id != teamDTO.ID)
            {
                return BadRequest();
            }

            var teamResult = await _teamRepository.PutTeam(id, teamDTO).ConfigureAwait(false);

            if (teamResult == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Teams
        [HttpPost]
        public async Task<ActionResult<TeamDTO>> PostTeam(TeamDTO teamDTO)
        {
            var teamResult = Ok(await _teamRepository.PostTeam(teamDTO).ConfigureAwait(false));

            return CreatedAtAction(nameof(GetTeam), new { id = teamDTO.ID }, teamDTO);
        }

        // DELETE: api/Teams/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TeamDTO>> DeleteTeam(int id)
        {
            var teamResult = await _teamRepository.DeleteTeam(id).ConfigureAwait(false);

            if (teamResult == null)
            {
                return NotFound();
            }

            return teamResult;
        }
    }
}
