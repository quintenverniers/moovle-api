﻿using Moovle.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.Repositories
{
    public interface IMedalRepository
    {
        Task<IEnumerable<MedalDTO>> GetMedals();
        Task<MedalDTO> GetMedal(int id);
        Task<MedalDTO> PostMedal(MedalDTO medalDTO);
        Task<MedalDTO> PutMedal(int id, MedalDTO medalDTO);
        Task<MedalDTO> DeleteMedal(int id);
    }
}
