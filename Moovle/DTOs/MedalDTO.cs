﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moovle.DTOs
{
    public class MedalDTO
    {
        public int ID { get; set; }

        [Required]
        public string MedalName { get; set; }

        public string MedalRequirements { get; set; }
        public ICollection<UserMedalDTO> UserMedals { get; set; }
    }
}
